# camera_calibration

Task 02: Camera Calibration

## The pinhole camera model
In chapter 7, we will find a description how 3D environment is projected onto
the 2D image.

Using the results of chapter 7, we will be able to project the 3D environment
onto the 2D image:

![3D to 2D](img/3d_to_2d.png)

For this projection, we defined the intrinsic matrix
𝐾 where

<img src="https://latex.codecogs.com/svg.image?K&space;:=&space;\begin{pmatrix}f&space;\cdot&space;k_1&space;&&space;0&space;&&space;s_1&space;\\0&space;&&space;f&space;\cdot&space;k_2&space;&&space;s_2&space;\\0&space;&&space;0&space;&&space;1&space;\\\end{pmatrix}&space;" /> 

The parameters 𝒇, 𝒌𝟏, 𝒌𝟐, 𝒔𝟏, and 𝒔𝟐 are defined
by the structure of the camera sensor itself, they
are called the inner or intrinsic parameters.


## Estimating the intrinsic matrix
This matrix can be estimated using recordings of a checkerboard.
Ideally, these recordings are done in different distances to get an overall good estimation (keep in mind, that the estimation works best if recordings are done at the same distance where we expect/want to detect objects).

## One more problem, though
But we realize another problem right away:
The camera used is not following the pinhole camera mode!

Objects with nice geometries are not represented as expected.
Here, the edges of the checkerboard and the edge of the cupboard are not on a line!

![checkerboard](img/checkerboard_line_problem.png)

## Lens distortion
This effect is called lens distortion (covered in chapter 8) and can also be tackled using the recordings with the checkerboard.
As we want to work with the same camera on task 3, we want to generate the undistorted images as well.

In our moodle course, you can find the images to the right as part of two videos provided in the folder “Camera Calibration”.
We will use these videos for this task.

# What we have to do
### Goals of task 2:
Use the video provided. Calibrate the camera and provide access to the undistorted images:
* Provide all intrinsic camera parameters.
* Provide the distortion coefficients and generate the undistorted images for at least four different images from the video. Compare them to the original and distorted images. Calculate the error of your calibration.
* reduce the count of chessboard corners, because the chessboard in the video has no white background (5x5 or 6x6, but 6x6 perform better)

Describe in your report how you did solve this problem, provide all information
needed to reproduce your algorithms. Include images where they are helpful to
describe your steps.


## Rules
Follow the general rules for a scientific report. Read the “How to cite” guide provided.

Maximum of 5 pages pure text (+5 optional pages of images). English only. Stay on topic.

Groups of two are allowed. If you want to work in a group of two, follow these additional rules:

1. First page of your report has to carry the information of both students involved (full name, student ID, study programm).
2. Both students have to upload the exact same pdf file individually via their own moodle account. If a student is listed on the first page but does not upload the compulsory report, the task will be graded with a 5.0.
