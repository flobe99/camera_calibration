\documentclass[bibliography=totoc]{scrartcl}
\usepackage[ngerman, english]{babel}
\usepackage{rwukoma}
\usepackage[pdfusetitle]{hyperref}
\usepackage{lipsum,caption}
\usepackage{acronym}
\usepackage{algorithm, algpseudocode}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{listings}
\usepackage{float}
\usepackage{todonotes}
\usepackage{amsmath}

\setlength{\belowcaptionskip}{5pt}

\title{Camera Calibration}
\author{Florian Betz - 35653, IN}
\date{\today}
\begin{document}
\maketitle
\tableofcontents

\clearpage
\section{Task Overview}

A human can detect a real environment with his eyes and recognize different distances.
There are already many technologies that can see the world in three dimensions.
A camera sees only in two dimensions, but has a higher spatial resolution than radar. \cite{cameraradar}
However, cameras cannot produce perfect images.
Depending on the tilt angle of the camera, the edges are shown stretched or compressed.
To correct the image distortion, we need to calibrate the camera.

In the following report, I will present an approach to calibrating a camera with a checkerboard.
Since I do not want to implement everything from scratch, I will use \ac{OpenCV}, an open source computer vision library suitable for our purpose.
It is available for C, C++, Python and Java \cite{OpenCV}.
I will use Python 3.9. My code and all images are available in the GitLab repository \cite{Gitlab}.

The algorithm consists of three main parts, each with its own challenges.
First, I have to create many different frames from a chessboard.
Then, I look for a predefined number of corners in each frame and detect the location of the corners in subpixels.
Finally, I calibrate the camera and calculate the new optimal camera matrix and the total error.

\section{Problem Definition}
The pinhole model can be used to project a 3D environment into a 2D image.
The problem with this transformation is that we reduce a 3D vector to a 2D image and lose some information.
When a figure is rotated in an image, the camera cannot project as it does in real life.
In the figure \ref{fig:cb_line_problem}, I draw a straight line through two corners.
You can see that not all edges are on the line.
This effect is due to the distortion caused by the camera lens.
To reduce and combat camera distortion, the camera should be calibrated.
To do this, you will need several test patterns.

\begin{figure}[H]
	\centering
	\begin{subfigure}[t]{0.25\linewidth}
		\includegraphics[width=\linewidth]{img/checkerboard_line_problem.png}
	\end{subfigure}
	\hspace{0.1\textwidth}
	\begin{subfigure}[t]{0.25\linewidth}
		\includegraphics[width=\linewidth]{img/checkerboard_line_problem_2.png}
	\end{subfigure}
	\caption{corner points do not lie on the same line at all}
	\label{fig:cb_line_problem}
\end{figure}

\section{Test patterns}
For camera calibration, you need a calibration grid.
There are different types of supported boards, but in our case we use a checkerboard because we want to calibrate a single camera.
The chessboard consists of 8x8 binary squares alternating with rectangular squares.

%\todo[inline]{create own images with a straight line, same pictures as the results in the last chapter}

I would like to improve this distortion.
My first goal is to get a checkerboard with a certain pattern size and a white border.
Now I need to create frames with different checkerboard areas.
To accomplish this task, I take a video of a chessboard movement and create a large number of frames.
The checkerboard should move in each corner and from background to foreground with different angles.
In my case, I have 4900 different frames.
One problem is that the calibration error decreases if you make frames only in one region.
But in new frames, when the checkerboard is in another region, the error increases and the image becomes distorted.

\section{Corner Detection} \label{corner_detection}

The images are converted from RGB space to grayscale to better see the checkerboard.
Then I am ready to find the chessboard corners using the open cv function \emph{cv.findChessboardCorners}.
This function detects a chessboard in an image and finds the pattern corners with two required parameters.
The first parameter is my grayscale image, but you can also use an RGB image.
The second parameter allows me to define the pattern size of the checkerboard as a row/column tuple.
However, this is not the number of squares in a row and column.
These are the inner corners.
My chessboard has a pattern of 8x8 squares. So the pattern of the inner corners is 7x7.
The function returns a validation value and an array with the recognized corners.
In the array are all corners with their 2d coordinates.
If the function finds all corners in the pattern, the validation value is set to true.
For each detected corner I need a corresponding world point.
We focus the coordinate grid only relative to the corners and add the grid as an array to a list.
The absolute 2D corner coordinates are added to another list.
To increase detection accuracy, I use the function \emph{cornerSubPix}.
This function creates a sub-pixel of each corner and returns the best corner position within a small neighborhood.
As input parameters I use the gray scaled image and all corners in a checkerboard.
Then I created a search window that the algorithm can use to find the corner.
The function needs half the length of the search window, so I set the parameter to 10x10.
Additionally, I can use a null zone, but my algorithm works fine without it.
So I set the parameter to (-1,-1), which means that there is no zero zone.
With the last parameter I can influence the iteration step of the algorithm by setting criterions.
The \emph{cv. TERM\_CRITERIA\_MAX\_ITER } I set the maximum number of iterations to 30 iterations and
\emph{cv.TERM\_CRITERIA\_EPS} I set the minimum precision to 0.001.
As output, I get a new corner list that I can use to draw the checkerboard corners.
This can be seen in figure \ref{fig:patternsize}.

\begin{figure}[H]
	\centering
	\begin{subfigure}[t]{0.28\linewidth}
		\includegraphics[width=\linewidth]{img/chessboard_with_corners_5x5.jpg}
		\caption{With a 5x5 inner corner pattern size}
		\label{subfig:5x5}
	\end{subfigure}
	\begin{subfigure}[t]{0.28\linewidth}
		\includegraphics[width=\linewidth]{img/chessboard_with_corners_6x6.jpg}
		\caption{With a 6x6 inner corner pattern size}
		\label{subfig:6x6}
	\end{subfigure}
	\begin{subfigure}[t]{0.28\linewidth}
		\includegraphics[width=\linewidth]{img/chessboard_with_corners_7x6.jpg}
		\caption{With a 7x6 inner corner pattern size}
		\label{subfig:7x6}
	\end{subfigure}
	\caption{Chessboard with different inner corner pattern size}
	\label{fig:patternsize}
\end{figure}

In the figure \ref*{subfig:7x6} you can see that opencv did not find a chessboard with a 7x7 inner chessboard.
The reason is that the chessboard has no white border to detect the whole chessboard.
To solve this problem, I can add a white border to the checkerboard or reduce the size of the pattern.
For this task, I choose the second approach.
A second reason is that the person moving the chessboard has his fingers on the pattern.
Or part of the checkerboard is outside the frame.
For a pattern size of 6x6, I get 1086 out of 4900 frames for successful corner detection.
To get more usable frames, I reduce the size of the pattern to 5x5.
With this pattern size I can use 2373 frames.
But for better camera calibration, the pattern should be as large as possible.
Preferably it should cover about half of the total area. \cite{calibration-best-practices}
The highest pattern size OpenCV can work with the existing images is 7x6.
With this pattern size, I can use 325 frames.
So I choose this pattern size to perform the calibration step.

\section{Camera Calibration}
\label{camera_calibration}
% Was habe ich vor mit der camera calibrierung?
% Wie habe ich es umgesetzt?
% Wo gab es Probleme? Wie habe ich die gelöst?
% Offene Themen ansprechen

The most important part of this task is the camera calibration.
As we saw in the previous section, we detect vertices in a 2D image.
Using the vertices, we compute the 3D world coordinates and can calibrate the camera.
The vertices are used to estimate the camera matrix.
With camera calibration, I can eliminate tangential and radial distortions.
The intrinsic and extrinsic parameters are also defined.
Distortion is divided into radial, see equation \ref*{eq:erl1} and \ref*{eq:erl2}, and tangential distortion in equations \ref*{eq:erl3} and \ref*{eq:erl4}.
Distortion occurs in the x and y directions.

radial distortion: (\cite{wu2017correction} p.3 equation 2)
\begin{align}
	\label{eq:erl1}
	x_{distortion} = x(1+k_1 r^2 + k_2 r^4 + k_3 r^6) \\ \
	\label{eq:erl2}
	y_{distortion} = y(1+k_1 r^2 + k_2 r^4 + k_3 r^6)
\end{align}
tangential distortion: (\cite{opencvcameracalibration} equation 3 and 4)
\begin{align}
	\label{eq:erl3}
	x_{distortion} = x + (2p_1 xy + p_2 (r^2 + 2x^2)) \\
	\label{eq:erl4}
	y_{distortion} = y + (p_1 (r^2 + 2y^2) + 2p_x xy)
\end{align}

In my calibration process, I have to define the optimal distortion coefficients to solve the equation.
The vector of the coefficients is defined as follow: \\
\begin{center}


	distortion coefficients = $\left( \begin{array}{rrrrr}
			k_1 & k_2 & p_1 & p_2 & k_3 \\
		\end{array}\right)^T$\\
\end{center}
As we can see in the computer vision lecture notes, we define the instrinsic matrix:
\begin{center}

	K := $\left( \begin{array}{rrr}
			f \cdot k_1 & 0           & s_1 \\
			0           & f \cdot k_2 & s_2 \\
			0           & 0           & 1   \\
		\end{array}\right)$
\end{center}


The parameters $f$, $k_1$, $k_2$, $s_1$, and $s_2$ are defined during calibration.
$k_1$ and $k_2$ are the distortion coefficient for radial distortion and $s_1$ and $s_2$ are coordinates of the center of the distortion.
A separate camera matrix is created for each image.
With the calculated distortion coefficients, image size, and camera matrices, we can find an optimal camera matrix for all images.
After calibration, the distortion is gone.

The calibration step of my camera is done with two OpenCV functions.
The first function is the \emph{cv.calibrateCamera} function.
It finds the intrinsic and extrinsic parameters using the corner detection from the section \ref{corner_detection}.
The first two input parameters are the world coordinates and the image plane coordinates.
I initialized the intrinsic matrix with the pixel size of my images.
The two optical parameters are an initialized intrinsic matrix and distortion coefficients.
I set the matrix to None because we are not setting a flag for a fixed aspect ratio.
The distortion coefficients are set to None because all coefficients are set to zero and need no further initialization.
The function returns a camera matrix, the distortion coefficients, a rotation vector and a translation vector.
The camera matrix and the distortion coefficients are returned as follows:

\begin{center}

	distortion coefficients = $\left( \begin{array}{rrrrr}
			k_1 \\ k_2 \\ p_1 \\ p_2 \\ k_3 \\
		\end{array}\right)$ =$\left( \begin{array}{rrrrr}
			-0.339353721 \\ 0.149692611 \\ -0.0001.00427196 \\ 0.000513041064\\ -0.0373157504 \\
		\end{array}\right)$\\
\end{center}

\begin{center}

	K = $\left( \begin{array}{rrr}
			f \cdot k_1 & 0           & s_1 \\
			0           & f \cdot k_2 & s_2 \\
			0           & 0           & 1   \\
		\end{array}\right)$ = $\left( \begin{array}{rrr}
			781.28875732 & 0            & 785.58385758 \\
			0            & 740.86645508 & 481.73001115 \\
			0            & 0            & 1            \\
		\end{array}\right)$
\end{center}


Given the condition that the calibrateCamera functions return valid values, I want to have an optimal camera matrix.
OpenCV has another function \\ \emph{cv.getOptimalNewCameraMatrix} that computes a new camera matrix with a scaling parameter.
I used this function to have different resolutions of a camera with the same calibration.
I use the camera matrix and distortion coefficients from the previous function as input parameters.
The function allows resizing the image.
In my case, I will not resize the image.
I use a threshold parameter alpha as a scaling parameter that can be used to remove or keep some pixels.
The function also returns a rectangle that outlines an area with all valid pixels of the undistorted image.

\section{Lens Distortion}
% Was habe ich vor mit der camera calibrierung?
% Wie habe ich es umgesetzt?
% Wo gab es Probleme? Wie habe ich die gelöst?
% Offene Themen ansprechen
In the final step of camera calibration, I want to combat image distortion.
As we can see in the \ref*{camera_calibration} section, we have already calculated the instrinsic matrix and distortion coefficients.
There are different types of distortion effects, see figure \ref*{fig:lensdistortion}
Due to radial distortion, straight lines in the real world appear curved on the image. \cite{lensdistortion}
Unlike the input raster, the lines are shifted inward or outward.
Barrel distortion corresponds to negative radial displacement and pincushion distortion corresponds to positive radial displacement.
\begin{figure}[H]	\centering
	\includegraphics[width = 0.75\textwidth]{img/radial-distortions-1024x352.png}
	\caption{Major types of distortion effects \cite{lensdistortion}}
	\label{fig:lensdistortion}
\end{figure}

Now we can reduce the lens distortion with the OpenCV function \emph{cv.undistor} and produce a corrected image.
As input parameters I use several frames, the calculated intrinsic camera matrix, the distortion coefficients and the optimal camera matrix.
This function corrects the lens distortion and returns an image with the same size as the input image.
In figure \ref{fig:pincushion-distortion} an undistorted image can be seen.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\linewidth]{img/chessboardcalibrate_pincushion.jpg}
	\caption{pincushion distortion}
	\label{fig:pincushion-distortion}
\end{figure}

In the figure \ref{fig:left-center-side} you can see a chessboard, which is on the left side of the image.
In the sub-image \ref{subfig:test_frame_0} one can see the image before calibration.
Not all edges are on one line.
But in the subimage \ref{subfig:chessboardcalibrate0},
which is a snapshot after the camera calibration, all edges are on the line.
\begin{figure}[H]
	\centering
	\begin{subfigure}[t]{0.28\linewidth}
		\includegraphics[width=\linewidth]{img/test_frame_0.jpg}
		\caption{Before camera calibration}
		\label{subfig:test_frame_0}
	\end{subfigure}
	\hspace{0.1\textwidth}
	\begin{subfigure}[t]{0.28\linewidth}
		\includegraphics[width=\linewidth]{img/chessboardcalibrate0.jpg}
		\caption{After camera calibration}
		\label{subfig:chessboardcalibrate0}
	\end{subfigure}
	\caption{Chessboard on the left-center side of the image}
	\label{fig:left-center-side}
\end{figure}

\clearpage
The image \ref{fig:center-side} is in the center of the image.
Before the calibration \ref{subfig:test_frame_2} almost all edges are on the line.
This has the background that the distortion in the center of the image is not so pronounced.

\begin{figure}[H]
	\centering
	\begin{subfigure}[t]{0.28\linewidth}
		\includegraphics[width=\linewidth]{img/test_frame_2.jpg}
		\caption{Before camera calibration}
		\label{subfig:test_frame_2}
	\end{subfigure}
	\hspace{0.1\textwidth}
	\begin{subfigure}[t]{0.28\linewidth}
		\includegraphics[width=\linewidth]{img/chessboardcalibrate2.jpg}
		\caption{After camera calibration}
		\label{subfig:chessboardcalibrate2}
	\end{subfigure}
	\caption{Chessboard in the center of the image}
	\label{fig:center-side}
\end{figure}

The distortion on the right side of the image, which you can see in \ref{fig:right-center-side}
and \ref{fig:right-side} is very strong.
However, calibration can reduce the distortion very well, as can be seen in the two partial images (b).


\begin{figure}[H]
	\centering
	\begin{subfigure}[t]{0.28\linewidth}
		\includegraphics[width=\linewidth]{img/test_frame_3.jpg}
		\caption{Before camera calibration}
		\label{subfig:test_frame_3}
	\end{subfigure}
	\hspace{0.1\textwidth}
	\begin{subfigure}[t]{0.28\linewidth}
		\includegraphics[width=\linewidth]{img/chessboardcalibrate3.jpg}
		\caption{After camera calibration}
		\label{subfig:chessboardcalibrate3}
	\end{subfigure}
	\caption{Chessboard on the right-center side of the image}
	\label{fig:right-center-side}
\end{figure}

\begin{figure}[H]
	\centering
	\begin{subfigure}[t]{0.28\linewidth}
		\includegraphics[width=\linewidth]{img/test_frame_5.jpg}
		\caption{Before camera calibration}
		\label{subfig:test_frame_5}
	\end{subfigure}
	\hspace{0.1\textwidth}
	\begin{subfigure}[t]{0.28\linewidth}
		\includegraphics[width=\linewidth]{img/chessboardcalibrate5.jpg}
		\caption{After camera calibration}
		\label{subfig:chessboardcalibrate5}
	\end{subfigure}
	\caption{Chessboard on the right side of the image}
	\label{fig:right-side}
\end{figure}


\section{Calibration Error}
After the distortion process is complete, I calculate the total error of the calibration.
The error tells how good the distortion coefficient estimate is.
If the error is closer to zero, the estimated coefficients are better.
I calculate the error using two OpenCV functions.
Using the function \emph{cv.projectPoints}, I want to calculate a 2D image point vector from my 3D world coordinates.
As input parameters I use the output parameters from the calibration step.
Another feature of a function is a Jacobian matrix, but I don't use it.
The second function \emph{cv.norm} calculates the absolute distance between the image points from the corner detection and the image point from the previous function.
To get the error of an image, I divided the distance by the length of the image point vector.
Then I add all errors of all images.
With a 7x6 chessboard pattern, I get an total error of 0.024328359969643797.

\clearpage

\section*{List of Acronyms}
\addcontentsline{toc}{section}{List of Acronyms}

\begin{acronym}[....]
	\acro{OpenCV}{Open Source Computer Vision Library}
\end{acronym}
\bibliographystyle{alpha}
\bibliography{literature}
\end{document}
