import cv2 as cv


def extractImages(pathIn, pathOut):
    vidcap = cv.VideoCapture(pathIn)
    success, image = vidcap.read()
    count = 0
    while success:
        cv.imwrite(pathOut + "frame%d.jpg" % count, image)  # save frame as JPEG file
        success, image = vidcap.read()
        print("Read a new frame: ", count)
        count += 1


if __name__ == "__main__":
    extractImages("video/video.h264", "frames/")
