from math import dist
from black import main
from cv2 import undistort
import numpy as np
import cv2 as cv
import glob
import os


def find_corners(CHECKERBOARD):

    objp = np.zeros((CHECKERBOARD[0] * CHECKERBOARD[1], 3), np.float32)
    objp[:, :2] = np.mgrid[0 : CHECKERBOARD[0], 0 : CHECKERBOARD[1]].T.reshape(-1, 2)

    criterias = (cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    world_3d_coords = []
    image_plane_2d = []

    for fname in glob.glob("frames/*"):
        print(fname)
        image = cv.imread(fname)
        gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)

        ret, corners = cv.findChessboardCorners(gray, CHECKERBOARD, None)
        print(corners)
        print("return value: ", ret)

        if ret == True:
            world_3d_coords.append(objp)
            image_plane_2d.append(corners)
            corners = cv.cornerSubPix(gray, corners, (10, 10), (-1, -1), criterias)

            if DEBUG:

                cv.imshow("gray", gray)
                draw = cv.drawChessboardCorners(image, CHECKERBOARD, corners, ret)
                cv.imshow("Find Chessboard", image)

                cv.waitKey(0)
        else:
            os.remove(fname)
    print("Number of chess boards find: ", len(image_plane_2d))

    return world_3d_coords, image_plane_2d


def calibrate_camera(world_3d_coords, image_plane_2d):

    img = cv.imread("test_frames/test_frame_0.jpg")
    h, w = img.shape[:2]  # height and weight of frames
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

    cv.matrix

    print("calibrateCamera")
    ret, cameraMatrix, distortion_coefficients, rvecs, tvecs = cv.calibrateCamera(
        world_3d_coords, image_plane_2d, gray.shape[::-1], None, None
    )
    print("getOptimalNewCameraMatrix")
    new_camera_matrix, roi = cv.getOptimalNewCameraMatrix(cameraMatrix, distortion_coefficients, (w, h), 1, (w, h))

    print("distortion coefficients", distortion_coefficients)
    print("Original Camera Matrix:\n", cameraMatrix)
    print("Optimal Camera Matrix:\n", new_camera_matrix)

    np.save(os.path.join("results", "Original camera matrix"), cameraMatrix)
    np.save(os.path.join("results", "Distortion coefficients"), distortion_coefficients)
    np.save(os.path.join("results", "Optimal camera matrix"), new_camera_matrix)

    return cameraMatrix, distortion_coefficients, rvecs, tvecs, new_camera_matrix, roi


def lens_distorbtion(cameraMatrix, distortion_coefficients, new_camera_matrix, roi):
    frame_count = 0
    print("lens distortion")
    for fname in glob.glob("test_frames/*"):
        img = cv.imread(fname)
        dst = cv.undistort(img, cameraMatrix, distortion_coefficients, None, new_camera_matrix)
        cv.imwrite(os.path.join("results", "undistortion", "chessboardcalibrate" + str(frame_count) + ".jpg"), dst)
        if DEBUG:
            cv.imshow("img", img)
            cv.imshow("dst", dst)
            cv.waitKey(0)
        frame_count += 1


def calc_error(world_3d_coords, image_plane_2d, rvecs, tvecs, cameraMatrix, distortion_coefficients):
    error = 0
    for i in range(len(world_3d_coords)):
        img_points, _ = cv.projectPoints(world_3d_coords[i], rvecs[i], tvecs[i], cameraMatrix, distortion_coefficients)
        error += cv.norm(image_plane_2d[i], img_points, cv.NORM_L2) / len(img_points)

    total_error = error / len(world_3d_coords)
    print("total error: {}".format(total_error))
    return total_error


if __name__ == "__main__":
    DEBUG = True
    CHECKERBOARD = (7, 6)

    world_3d_coords, image_plane_2d = find_corners(CHECKERBOARD)

    cameraMatrix, distortion_coefficients, rvecs, tvecs, new_camera_matrix, roi = calibrate_camera(
        world_3d_coords, image_plane_2d
    )

    lens_distorbtion(cameraMatrix, distortion_coefficients, new_camera_matrix, roi)

    total_error = calc_error(world_3d_coords, image_plane_2d, rvecs, tvecs, cameraMatrix, distortion_coefficients)

    cv.waitKey(0)
    cv.destroyAllWindows()
